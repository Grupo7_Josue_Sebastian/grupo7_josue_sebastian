#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int *getRandoM(){

	static int r[10];
	int i;

	srand(time(NULL));

	for (i=0;i<10;++i){
		r[i] = rand();
		printf("r[%d] = %d \n", i, r[i]);
	}
	return r;
}

int main(){

	int *p;
	int i;

	p = getRandoM();

	for (i=0;i<10;i++){
		printf("*(p + %d) : %d\n", i, *(p+i));
	}
	return 0;
}


