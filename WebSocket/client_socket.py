import socket

HEADER = 64
PORT = 3074
SERVER = '192.168.0.4'
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = 'DISCONNECT!'

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

client.connect(ADDR)

def send(msg):
    message=msg.encode(FORMAT)

    msg_length = len(message)
    send_length = str(msg_length).encode(FORMAT)

    send_length += b' '*(HEADER-len(send_length))

    client.send(send_length)
    client.send(message)
    print(client.recv(2048).decode(FORMAT))

send('Mensaje no autorizado')
input()

send('0C-9D-92-C8-C5-DF')
input()

send('mensaje autorizado')
input()
send(DISCONNECT_MESSAGE)