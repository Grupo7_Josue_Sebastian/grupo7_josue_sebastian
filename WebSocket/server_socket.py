import socket
import threading

HEADER = 64
PORT = 3074
SERVER = '192.168.0.4'
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = 'DISCONNECT!'
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

server.bind(ADDR)




def handle_client(conn, addr):
    print(f"[NEW CONNECTION] {addr} connected.")
    
    connected = True
    flaghs=0
    while connected:
        msg_length = conn.recv(HEADER).decode(FORMAT)
        if msg_length:
            msg_length = int(msg_length)
            msg = conn.recv(msg_length).decode(FORMAT)
            if msg == '0C-9D-92-C8-C5-DF':
                flaghs=1
                conn.send('Autorizado'.encode(FORMAT))
                
            if flaghs == 1:
                if msg == DISCONNECT_MESSAGE:
                    connected = False
    
                print(f"[{addr}] {msg}")
                conn.send("Msg received".encode(FORMAT))
                
            else:
                conn.send("Mensaje no autorizado".encode(FORMAT))
    conn.close()


def start():
    server.listen()
    print(f"[LISTEN] Server is listening on address {ADDR}")
    while True:
        conn, addr = server.accept()
        thread = threading.Thread(target=handle_client, args=(conn, addr))
        thread.start()
        print(f"[ACTIVE CONNECTIONS] {threading.activeCount() - 1}")

print("[STARTING] server is running.....")
start()